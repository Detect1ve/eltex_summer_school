#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>

struct mymsgbuf {
	long mtype;
	int mtext;
};

int msgqid, rc;

void send_message(int qid, struct mymsgbuf *qbuf, long type, int text) {
	qbuf->mtype = type;
	qbuf->mtext = text;
	if((msgsnd(qid, (struct msgbuf *)qbuf, sizeof(*qbuf) - sizeof(long), 0)) == -1) {
		perror("msgsnd");
		exit(1);
	}
}

void read_message(int qid, struct mymsgbuf *qbuf, long type, int *global) {
	qbuf->mtype = type;
	msgrcv(qid, (struct msgbuf *)qbuf, sizeof(int), type, 0);
	*global = qbuf->mtext;
}

int main(int argc, char *argv[]) {
	pid_t main_pid = getpid();
	key_t key;

        struct mymsgbuf qbuf;

        int i;
	int status;
	int n;
	int global;
        int pid;

	int qtype = 1;
	int step = 50;

	puts("Введите количество процессов");
	scanf("%d", &n);
	puts("Введите количество золота в шахте");
	scanf("%d", &global);

	key = ftok(".", 'm');
	if((msgqid = msgget(key, IPC_CREAT|0660)) == -1) {
		perror("msgget");
		exit(1);
	}

	send_message(msgqid, (struct mymsgbuf *)&qbuf, qtype, global);

	for (i = 1; i < n; i++) {
		pid = fork();
		srand(getpid());
		if (-1 == pid) {
			perror("fork");
			exit(1);
		} else if (0 == pid) {
			while (global > 0) {
				sleep(rand() % 4);
				read_message(msgqid, &qbuf, qtype, &global);
				if (global <= 0) {
					send_message(msgqid, (struct mymsgbuf *)&qbuf, qtype, global);
					exit(0);
				}
				global -= step;
				printf("Процесс %d забрал из шахты %d золота и там осталось %d\n", getpid() - main_pid, step, global);
				fflush(stdout);
				send_message(msgqid, (struct mymsgbuf *)&qbuf, qtype, global);
			}
			exit(0);
		}
	}

	for (i = 1; i < n; i++)
		status = wait(NULL);

	if ((rc = msgctl(msgqid, IPC_RMID, NULL)) < 0) {
		perror( strerror(errno) );
		printf("msgctl (return queue) failed, rc=%d\n", rc);
		return 1;
	}
	return 0;
}
