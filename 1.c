#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

void scan(char** string, int count)
{
	for (int i = 0; i < count; i++)
	scanf("%s", string[i]);
}

void print(char** string, int count)
{
	puts("А вот сортировочка прошла");
	for (int i = 0; i < count; i++)
	printf("%s\n", string[i]);
}

void sort(char** in, char** out, int count)
{
	int *b;
	b = calloc(sizeof(int), count);
	int *len;
	len = malloc(sizeof(int) * count);
	for (int i = 0; i < count; i++)
		len[i] = strlen(in[i]);
	//Определение количества цифр во всех строках
	for (int i = 0; i < count; i++)
		for (int j = 0; j < len[i]; j++)
			if (in[i][j] > 47 && in[i][j] < 58)
				b[i]++;
	//Сортировка
	int tmp = 0;
	for (int i = 0; i < count; i++)
	{
		int max = INT_MIN, nmax = INT_MIN;
		for (int j = 0; j < count; j++)
			if (max <= b[j])
			{
				max = b[j]; nmax = j;
			}
		out[tmp] = in[nmax];
		tmp++;
		b[nmax] = -1;
	}
	free(b);
	free(len);
}

int main(int argc, char **argv[])
{
	int count;
	char **in, **out;
	puts("Введите количество строк");
	scanf("%d", &count);
	puts("Введите сами строки");
	//Выделение памяти
	in = malloc(sizeof(char *) * count);
	out = malloc(sizeof(char *) * count);
	for (int i = 0; i < count; i++)
	{
		in[i] = calloc(sizeof(char), 80);
		out[i] = calloc(sizeof(char), 80);
	}
	//Ввод данных
	scan(in, count);
	//Сортировка
	sort(in, out, count);
	//Вывод
	print(out, count);
	/*for (int i = 0; i < count; i++)
	{
		free(in[i]);
		free(out[i]);
	}*/
	free(in);
	free(out);
	return 0;
}
