#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

void print(int **array, int *size, int n) {
        for (int i = 0; i < n; i++) {
		printf("Массив № %d = ", i);
                for (int j = 0; j < size[i]; j++)
                        printf("%d\t", array[i][j]);
                puts("");
        }
	fflush(stdout);
}

int main()
{
	int n;
	int **array;
	int *size;
	puts("Введите количество массивов");
	scanf("%d", &n);
	int fd[n][2];
	size = malloc(sizeof(int) * n);
	array = malloc(sizeof(int *) * n);
	for (int i = 0; i < n; i++) {
		printf("Введите количество элементов для %d массива ", i);
		scanf("%d", &size[i]);
		array[i] = malloc(sizeof(int) * size[i]);
		for (int j = 0; j < size[i]; j++)
			array[i][j] = rand() % 10;
	}

	print(array, size, n);

	pid_t pid;
	int tmp = getpid();
        int proc_num = 0;
	for (int i = 1; i < n; i++)
		if (getpid() - tmp == 0) {
			pipe(fd[i]); 
			pid = fork();
			if (pid == 0)
				proc_num = i;
		}

	float math_wait = 0;
	for (int j = 0; j < size[proc_num]; j++)
		math_wait += array[proc_num][j];
	math_wait /= size[proc_num];
	for (int i = 1; i < n; i++)
		if (pid == 0 && i == proc_num) {
			close(fd[i][0]);
			write(fd[i][1], &math_wait, sizeof(float));
			exit(0);
		}
	printf("Мат ожидание массива %d равно %f\n", getpid() - tmp, math_wait);
	fflush(stdout);
	wait(NULL);

	for (int i = 1; i < n; i++) {
		close(fd[i][1]);
		float out;
		read(fd[i][0], &out, sizeof(float));
		printf("Мат ожидание массива %d равно %f\n", i, out);
		fflush(stdout);
	}
	for (int i = 0; i < n; i++)
		free(array[i]);
	free(array);
	free(size);
	return 0;
}
