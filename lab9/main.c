#include <sys/shm.h>
#include <wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#include <sys/sem.h>

union semun {
 int val;                  /* значение для SETVAL */
 struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
 unsigned short *array;    /* массивы для GETALL, SETALL */
                           /* часть, особенная для Linux: */
 struct seminfo *__buf;    /* буфер для IPC_INFO */
};

int main(int argc, char *argv[]) {
	pid_t main_pid = getpid();
	pid_t pid;

	int step = 50;

	int n;
	int global;
	int shmid;
	int semid;
	int *shm;
	int i;

	union semun arg;

	struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
	struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса

	key_t key = 69;

	puts("Введите количество процессов");
        scanf("%d", &n);
        puts("Введите количество золота в шахте");
        scanf("%d", &global);

	if ((key = ftok(".", 'S')) < 0) {
		printf("Невозможно получить ключ\n");
		exit(1);
	}

	semid = semget(key, 1, 0666 | IPC_CREAT);

	arg.val = 1;
	semctl(semid, 0, SETVAL, arg);

	if ((shmid = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0666)) < 0) {
		perror("shmget");
		exit(1);
	}

	if ((shm = shmat(shmid, NULL, 0)) == (int *) -1) {
		perror("shmat");
		exit(1);
	}
	*(shm) = global;
	if (shmdt(shm) < 0) {
		printf("Ошибка отключения\n");
		exit(1);
	}

	for (i = 1; i < n; i++) {
		pid = fork();
		srand(getpid());
		if (-1 == pid) {
			perror("fork");
			exit(1);
		} else if (0 == pid) {
			if ((shm = shmat(shmid, NULL, 0)) == (int *) -1) {
				perror("shmat");
				exit(1);
			}
			while (*(shm) > 0) {
				if((semop(semid, &lock_res, 1)) == -1){
					fprintf(stderr, "Lock failed\n");
					exit(1);
				}
				sleep(rand() % 2);
				if (*(shm) <= 0) {
	                                if (shmdt(shm) < 0) {
        	                                printf("Ошибка отключения\n");
                	                        exit(1);
                        	        }
	                                if((semop(semid, &rel_res, 1)) == -1){
	                                        fprintf(stderr, "Unlock failed\n");
        	                                exit(1);
					}
					exit(0);
				}
				*(shm) -= step;
				printf("Процесс %d забрал из шахты %d золота и там осталось %d\n", getpid() - main_pid, step, *(shm));
				fflush(stdout);
				if((semop(semid, &rel_res, 1)) == -1){
					fprintf(stderr, "Unlock failed\n");
					exit(1);
				}
			}
			if (shmdt(shm) < 0) {
				printf("Ошибка отключения\n");
				exit(1);
			}

			exit(0);
		}
	}
	fflush(stdout);
	for (i = 1; i < n; i++)
		wait(NULL);
	if (shmctl(shmid, IPC_RMID, 0) < 0) {
                printf("Невозможно удалить область\n");
                exit(1);
	}
	return 0;
}
