#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

void print(int **array, int *size, int n) {
        for (int i = 0; i < n; i++) {
		printf("Массив № %d = ", i);
                for (int j = 0; j < size[i]; j++)
                        printf("%d\t", array[i][j]);
                puts("");
        }
	fflush(stdout);
}

int main()
{
	int n;
	int **array;
	int *size;
	puts("Введите количество массивов");
	scanf("%d", &n);
	size = malloc(sizeof(int) * n);
	array = malloc(sizeof(int *) * n);
	for (int i = 0; i < n; i++) {
		printf("Введите количество элементов для %d массива ", i);
		scanf("%d", &size[i]);
		array[i] = malloc(sizeof(int) * size[i]);
		for (int j = 0; j < size[i]; j++)
			array[i][j] = rand() % 10;
	}

	print(array, size, n);

	pid_t pid;
	int tmp = getpid();
	int proc_num = 0;
	for (int i = 1; i < n; i++)
		if (getpid() - tmp == 0) {
			pid = fork();
			if (pid == 0)
				proc_num = i;
		}

	float math_wait = 0;
	for (int j = 0; j < size[proc_num]; j++)
		math_wait += array[proc_num][j];
	math_wait /= size[proc_num];

	printf("Мат ожидание массива %d равно %f\n", proc_num, math_wait);
	fflush(stdout);

	if (pid == 0)
		exit(-1);
	wait(NULL);

	for (int i = 0; i < n; i++)
		free(array[i]);
	free(array);
	free(size);
	return 0;
}
