#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <time.h>

#define size 100

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "You didn't enter the server address\n");
		exit(EXIT_FAILURE);
	}
	if (argc < 3) {
		fprintf(stderr, "You didn't enter a port number\n");
		exit(EXIT_FAILURE);
	}

	int portno;
	struct hostent *server;
	portno = atoi(argv[2]);
	server = gethostbyname(argv[1]);

	srand(time(NULL));
	int bytes_read;
	char *message;
	int sock;
	struct sockaddr_in addr;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		perror("socket");
		exit(1);
	}
	message = malloc(sizeof(char) * size);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(portno);
	bcopy((char *)server->h_addr, (char *)&addr.sin_addr.s_addr, server->h_length);
	char buf[100];
	if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("connect");
		exit(2);
	}
	int rand_count;
	int kill;
	int flag = 1;
	while (flag > 0) {
		bytes_read = recv(sock, buf, size, 0);
		buf[bytes_read] = 0;
		printf("%s\n", buf);
		if (strcmp(buf, "win") == 0) {
			printf("Я выиграл\n");
			return 0;
		}
		rand_count = atoi(buf);
		if (rand_count == -1) {
			flag = 0;
			printf("Я умираю\n");
		}
		sleep(5);
		kill = rand() % rand_count;
		sprintf(message, "%d", kill);
		if (flag > 0)
			send(sock, message, strlen(message), 0);
	}

	close(sock);

	return 0;
}
