#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>

#define ERROR_INIT_BARRIER    -14
#define ERROR_WAIT_BARRIER    -15
#define size 1024
struct thread_data {
	pthread_t tid;
	int threadno;	// Порядковый номер потока 0, 1, 2, ...
	int nthreads;	// Количество потоков
	int listener;
	pthread_t *tids;
};

pthread_mutex_t mutex_sum = PTHREAD_MUTEX_INITIALIZER;
pthread_barrier_t barrier;

int *array;			//clients status (0 - alive, 1 - dead)
int thread_count;	//number of threads

void *thread_func(void *arg)
{
	struct sockaddr their_addr;	//-----
	socklen_t sin_size;			//for accept
	int client_sock;			//-----
	int bytes_read;
	char buf[size];
	char answer[size];
	struct thread_data *p = (struct thread_data*)arg;
	int status;					//for barrier synchronization
	int kill;
	//random seed
	srand(p->threadno);

	while(1)
	{
		client_sock = accept(p->listener, (struct sockaddr *)&their_addr, &sin_size);
		if (client_sock < 0) {
			perror("accept");
			exit(3);
		}
		while(1) {
			status = pthread_barrier_wait(&barrier);
	        fflush(stdout);
			if (thread_count < 2)
				exit(1);
			if (array[p->threadno] != 1) {
	            pthread_mutex_lock(&mutex_sum);
	            for (int i = 0; i < p->nthreads; i++)
	            	if (p->threadno >= thread_count)
	            		if (array[i] == 1) {
							printf("Сейчас будет смена\n");
							fflush(stdout);
							p->threadno = i;
							array[p->threadno] = 1;
	            			array[i] = 0;
	            		}
	            pthread_mutex_unlock(&mutex_sum);

				if (p->threadno == 0)
					printf("Все в сборе и все готовы убивать!\n");
				fflush(stdout);
				sleep(5);
				sprintf(answer, "%d", thread_count);
				send(client_sock, answer, strlen(answer), 0);
				bytes_read = recv(client_sock, buf, 100, 0);
	            buf[bytes_read] = 0;
	            printf("%d заказал %s\n", p->threadno, buf);
				fflush(stdout);
				kill = atoi(buf);
				if (kill == p->threadno) {
					printf("%d хочет убить сам себя\n", p->threadno);
					fflush(stdout);
				} else {
					pthread_mutex_lock(&mutex_sum);
					array[kill] = 1;
					thread_count--;
					for (int i = 0; i < thread_count; i++) {
						printf("%d изменил массив и теперь он такой %d\n", p->threadno, array[i]);
						fflush(stdout);
					}
					pthread_mutex_unlock(&mutex_sum);
				}
				printf("%d до внутреннего барьера\n", p->threadno);
				fflush(stdout);
				status = pthread_barrier_wait(&barrier);
				printf("%d после внутреннего барьера\n", p->threadno);
	     	  	fflush(stdout);
				pthread_mutex_lock(&mutex_sum);
				if (array[p->threadno] == 1) {
					printf("%d умирает\n", p->threadno);
					fflush(stdout);
					sprintf(answer, "%d", -1);
					send(client_sock, answer, strlen(answer), 0);
				} else if ((array[p->threadno] == 0) && (thread_count == 1)) {
						printf("%d выиграет\n", p->threadno);
						fflush(stdout);
						sprintf(answer, "%s", "win");
						send(client_sock, answer, strlen(answer), 0);
				}
				pthread_mutex_unlock(&mutex_sum);
			}
		}
		close(client_sock);
	}
	return NULL;
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "You didn't enter the server address\n");
		exit(EXIT_FAILURE);
	}
	if (argc < 3) {
		fprintf(stderr, "You didn't enter a port number\n");
		exit(EXIT_FAILURE);
	}
	struct hostent *server;		//server address
	int portno;					//port number
	int yes = 1;				//for setsockopt
	int listener;				//for socket
	struct sockaddr_in addr;	//for bind
	int count;					//number of clients
	int status;					//for barrier synchronization
	//getting server address
	server = gethostbyname(argv[1]);
	//getting port number
	portno = atoi(argv[2]);
	//getting number of clients
	printf("Enter the number of clients ");
	scanf("%d", &count);

	thread_count = count;

	//memory allocation
	array = calloc(sizeof(int), count);
	pthread_t *tids = malloc(sizeof(*tids) * count);
	struct thread_data *tdata = malloc(sizeof(*tdata) * count);

	listener = socket(AF_INET, SOCK_STREAM, 0);
	if (listener < 0) {
		perror("socket");
		exit(1);
	}
	//reuse address
	if (setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	addr.sin_family = AF_INET;
	addr.sin_port = htons(portno);
    bcopy((char *)server->h_addr, (char *)&addr.sin_addr.s_addr, server->h_length);

	if (bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
	        perror("bind");
        	exit(2);
	}

	listen(listener, count);
	//creature barrier
	status = pthread_barrier_init(&barrier, NULL, count);

	for (int i = 0; i < count; i++) {
		tdata[i].threadno = i;
		tdata[i].nthreads = thread_count;
		tdata[i].listener = listener;
		if (pthread_create(&tids[i], NULL, thread_func, &tdata[i]) != 0) {
			fprintf(stderr, "Can't create thread\n");
			exit(EXIT_FAILURE);
		}
	}
	for (int i = 0; i < count; i++)
		pthread_join(tids[i], NULL);
	free(array);
	free(tdata);
	free(tids);
	return 0;
}
