#include <stdio.h>
#include <stdlib.h>

#define N 4
#define long_name 100

struct eight
{
	char name[long_name];
	int _long;
	int count;
	int cost;
};

void sort(struct eight **tmp)
{
	struct eight *swap;
	for (int i = 0 ; i < N - 1; i++)
		for (int j = 0 ; j < N - i - 1 ; j++)
			if(tmp[j]->cost < tmp[j + 1]->cost)
			{
				swap = tmp[j];
				tmp[j] = tmp[j + 1];
				tmp[j + 1] = swap;
			}
}

int main()
{
	struct eight *task[N];
	for (int i = 0; i < N; i++)
		task[i] = malloc(sizeof(struct eight));
	for (int i = 0; i < N; i++)
	{
		if (i == 0) 
		{
			puts("Введите название маршрута");
			scanf("%s", task[i]->name);
			puts("Введите протяжённость маршрута");
			scanf("%d", &(task[i]->_long));
			puts("Введите количество отсановок");
			scanf("%d", &(task[i]->count));
			puts("Введите стоимость путёвки");
			scanf("%d", &(task[i]->cost));
		}
		else
		{
                        scanf("%s", task[i]->name);
                        scanf("%d", &(task[i]->_long));
                        scanf("%d", &(task[i]->count));
                        scanf("%d", &(task[i]->cost));
		}
	}
	sort(task);
	for (int i = 0; i < N; i++)
	{
		printf("Маршрут %s\n", task[i]->name);
		printf("Протяжённость %d\n", task[i]->_long);
		printf("Количество остановок %d\n", task[i]->count);
		printf("Стоимость %d\n", task[i]->cost);
	}
	for (int i = 0; i < N; i++)
		free(task[i]);
	return 0;
}
