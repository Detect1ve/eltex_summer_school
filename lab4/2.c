#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define long_string 100

int main(int argc, char *argv[])
{
	FILE *f;
        int count;
        char **str;

        if (argc < 3) {
                fprintf(stderr, "Мало аргументов");
                exit(1);
        }

        if ((f = fopen(argv[1], "r")) == NULL) {
                puts("Не удаётся открыть файл");
                exit(1);
        }
        fscanf(f, "%d", &count);
        str = malloc(sizeof(char*) * count);
        for (int i = 0; i < count; i++)
                str[i] = malloc(sizeof(char) * long_string);
        for (int i = 0; i < count; i++)
                fgets(str[i], long_string, f);
        if (fclose(f)) {
                printf("Произошла ошибка при закрытии файла");
                exit(1);
        }

	if ((f = fopen(argv[1], "w")) == NULL) {
		puts("Не удаётся открыть файл");
		exit(1);
	}
	for (int i = 1; i < count; i++)
		for (int j = 0; j < strlen(str[i]); j++)
			if (str[i][j] == argv[2][0])
				str[i][j] = ' ';
	fprintf(f, "%d\n", count);
	for (int i = 1; i < count; i++)
		fprintf(f, "%s", str[i]);
	if (fclose(f)) {
		printf("Произошла ошибка при закрытии файла");
		exit(1);
	}
	return 0;
}
