#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>

#include "myqueue.h"

char const SERVER_NAME[16] = "127.0.0.1";
char const CLIENT_NAME[16] = "127.255.255.255";
int const SERVER_PORT_TCP = 50231;
int const SERVER_PORT_UDP = 60231;
int const CLIENT1_PORT_UDP = 61231;
int const CLIENT2_PORT_UDP = 61241;
int const CLIENT1_PORT = 50232;
int const CLIENT2_PORT = 50233;
int const MAX_PORT = 6;
int const SERVER_READY = 1;
int const SERVER_ANSWER = 2;
int const MAX_MES = 8;
int const WAIT_L = 5;
int const WAIT_K = 2;
int const CLIENT_TCP1 = 1;
int const CLIENT_TCP2 = 2;
int const backlog = 5;

int nclients = 0;

void error(const char *msg)	// Error processing
{
	perror(msg);
	exit(1);
}

void printusers()	// Number of active users
{
	if(nclients)
		printf("%d user on-line\n", nclients);
	else
		printf("No user on line\n");
}

int init_server()
{
	int sockfd;	// Socket descriptors
	int portno;	// Port number
	struct sockaddr_in serv_addr;	// Server socket structure
	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sockfd < 0) {	// Error creating socket
		error("ERROR opening socket");
		exit(EXIT_FAILURE);
	}

	memset((char *) &serv_addr, '\0', sizeof(serv_addr));
	portno = SERVER_PORT_TCP;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;	// Server accepts connections to all IP addresses
	serv_addr.sin_port = htons(portno);

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {	// Binding a socket to a local address
		error("ERROR on binding");
		exit(EXIT_FAILURE);
	}

	return sockfd;
}

int server_udp_L(int sock, int qid, int argc, char *argv[])
{
	// UDP socket configuration
	struct sockaddr_in addr;
	struct hostent *server;
	char buf[WORD_LEN];
	int i = 0;
	int j;

	if(argc > 1)
		server = gethostbyname(argv[1]);
	else
		server = gethostbyname(CLIENT_NAME);

	memset((char *) &addr, '\0', sizeof(addr));
	addr.sin_family = AF_INET;
	memcpy((char *)server->h_name, (char *)&addr.sin_addr.s_addr, server->h_length);
	addr.sin_port = htons(CLIENT2_PORT_UDP);
	strcpy(buf, "Есть сообщение\n");

	while(i < 3) {
		j = 0;
		sleep(WAIT_L);

		if (sum_mes_que(qid) > 0) {	// Find out the number of messages in the queue

			while(j <= MAX_PORT) {
				addr.sin_port = htons(CLIENT2_PORT_UDP + j);

				if(sendto(sock, &buf, strlen(buf), 0, (struct sockaddr *)&addr, sizeof(addr)) <= 0) {	//Send the message in buf to the server
					error("Something went wrong");
					return EXIT_FAILURE;
				}

				j++;
			}

			printf("UDP distribution for clients of the second type\n");
		}

	}

	return 0;
}

int server_udp_K(int sock, int qid, int argc, char *argv[])
{
	// UDP socket configuration
	struct sockaddr_in addr;
	struct hostent *server;
	char buf[WORD_LEN];
	int i = 0;
	int j;

	if(argc > 1)
		server = gethostbyname(argv[1]);
	else
		server = gethostbyname(CLIENT_NAME);

	memset((char *) &addr, '\0', sizeof(addr));
	addr.sin_family = AF_INET;
	memcpy((char *)server->h_name, (char *)&addr.sin_addr.s_addr, server->h_length);
	addr.sin_port = htons(CLIENT1_PORT_UDP);
	strcpy(buf, "Жду сообщений\n");

	while(i < 3) {
		j = 0;
		sleep(WAIT_K);

		if(sum_mes_que(qid) < MAX_MES) {	// Find out the number of messages in the queue

			while(j <= MAX_PORT) {
				addr.sin_port = htons(CLIENT1_PORT_UDP + j);

				if(sendto(sock, &buf, strlen(buf), 0, (struct sockaddr *)&addr, sizeof(addr)) <= 0) {	// Send the message in buf to the server
					error("Something went wrong");
					return EXIT_FAILURE;
				}

				j++;
			}

			printf("UDP distribution for clients of the first type\n");
		}

	}

	return 0;
}

void client2(int sock, int qid)	// Send message
{
	uint8_t buf[MAX_MSG_SIZE];
	void *vbuf;
	Packet mess = PACKET__INIT;
	struct mymsgbuf *mes;
	int len;
	struct mymsgbuf qbuf;
	long qtype = sizeof(struct mymsgbuf) - sizeof(long);
	printf("The server works with the second type of client.\n");

	if(sum_mes_que(qid) > 0) {
		mes = read_message(qid, &qbuf, qtype);
		printf("Server send message: time = %d, length = %d\nstring = %s\n", mes->time, mes->len, mes->str);
		mess.time = mes->time;
		mess.name.len = mes->len;
		mess.name.data = mes->str;
		mess.str = malloc(WORD_LEN);
		strcpy(mess.str, mes->str);
		// Sending message and packing structure
		len = packet__get_packed_size(&mess);
		vbuf = malloc(len);
		packet__pack(&mess,  vbuf);
		send(sock, vbuf, len, 0);
	} else {
		printf("Empty queue\n");
		send(sock, &buf, 0, 0);
		printf("Server send error: Empty queue\n");
	}

	nclients--;	// Reduce the active clients counter
	printf("The second type of client disconnected\n");
	printusers();
	return;
}

void client1(int sock, int qid)	// Receive message
{
	printf("The server works with the first type of client.\n");
	Packet *mes;
	uint8_t buf[MAX_MSG_SIZE];
	struct mymsgbuf qbuf;
	int bytes_recv, k;
	bytes_recv = recv(sock, &buf, sizeof(buf), 0);

	if(bytes_recv < 0) {
		error("ERR reading socket");
		exit(EXIT_FAILURE);
	}

	mes = packet__unpack(NULL, bytes_recv, buf);
	printf("Server received message: time = %d, lenght = %d\nstring = %s\n", mes->time, (int)mes->name.len, mes->name.data);
	// Send the message to the queue
	long qtype = sizeof(struct mymsgbuf) - sizeof(long);
	k = sum_mes_que(qid);

	if (k < MAX_MES)
		send_message(qid, (struct mymsgbuf *)&qbuf, qtype, mes);

	//-----
	nclients--;	// Reduce the active clients counter
	printf("The first type of client disconnected\n");
	printusers();
	return;
}

int init_udp()
{
	int sockfd;	// Socket descriptors
	int portno;	// Port number
	struct sockaddr_in serv_addr;	// Server socket structure
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	if (sockfd < 0) {	// Error creating socket
		error("ERROR opening socket");
		exit(EXIT_FAILURE);
	}

	memset((char *) &serv_addr, '\0', sizeof(serv_addr));
	portno = SERVER_PORT_UDP;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;	// Server accepts connections to all IP addresses
	serv_addr.sin_port = htons(portno);

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {	// binding the socket to the local address
		error("ERROR on binding");
		exit(EXIT_FAILURE);
	}

	return sockfd;
}

int init_queue()
{
	int msgqid;
	printf("Creating the queue\n");
	struct msqid_ds info;
	key_t key;
	key = ftok(".", 'm');

	if((msgqid = msgget(key, IPC_CREAT|0660)) == -1) {
		printf("Can't create queue\n");
		perror("msgget");
		exit(1);
	}

	msgctl(msgqid, IPC_STAT, &info);
	printf("Queued messages - %d\n", (int)info.msg_qnum);

	if((int)info.msg_qnum > 0) {
		msgctl(msgqid, IPC_RMID, &info);

		if((msgqid = msgget(key, IPC_CREAT|0660)) == -1) {
			printf("Can't create queue\n");
			perror("msgget");
			exit(1);
		}

		msgctl(msgqid, IPC_STAT, &info);
		printf("Queued messages - %d\n", (int)info.msg_qnum);
	}

	return msgqid;
}

int main(int argc, char *argv[])
{
	int msgqid;
	struct sockaddr_in cli_addr;
	socklen_t clilen;	// size of client address type socklen_t
	int sock_tcp, newsockfd, sock_udp, flag_udp, flag_mes, n;
	int pid;	// Process id
	msgqid = init_queue();	// Creating the queue
	// Creation of processes that carry UDP mailing
	sock_udp = init_udp();
	pid = fork();	// Creating the UDP K mailing process

	if (pid < 0) {
		error("ERROR on fork");
		exit(EXIT_FAILURE);
	}

	if (pid == 0) {	// Closed the mailing list for the first type of client
		flag_udp = server_udp_K(sock_udp, msgqid, argc, argv);

		if (flag_udp < 0)
			error("ERROR in UDP K");

		close(sock_udp);
		return 0;
	}

	pid = fork();	// Creating the UDP L mailing process

	if (pid < 0) {
		error("ERROR on fork");
		exit(EXIT_FAILURE);
	}

	if (pid == 0) {	// Closed the mailing list for the second type of client
		flag_udp = server_udp_L(sock_udp, msgqid, argc, argv);

		if (flag_udp < 0)
			error("ERROR in UDP L");

		close(sock_udp);
		return 0;
	}

	close(sock_udp);
	sock_tcp = init_server();
	listen(sock_tcp, backlog);	// Waiting for connections, connection requests to socket at the same time - 5
	clilen = sizeof(cli_addr);
	// Retrieving message from queue

	while (1) {	// Cycle to retrieve connection requests from the queue
		newsockfd = accept(sock_tcp,(struct sockaddr *) &cli_addr, &clilen);

		if (newsockfd < 0) {
			error("ERROR on accept");
			exit(EXIT_FAILURE);
		}

		pid = fork();

		if (pid < 0) {
			error("ERROR on fork");
			exit(EXIT_FAILURE);
		}

		if (pid == 0) {
			close(sock_tcp);
			nclients++;	// Increment the counter
			printusers();
			n = recv(newsockfd, &flag_mes, sizeof(flag_mes), 0);
			printf("Server: port client = %d, type client = %d\n", cli_addr.sin_port, flag_mes);	// Client information display

			if(flag_mes == CLIENT_TCP1)
				client1(newsockfd, msgqid);

			if(flag_mes == CLIENT_TCP2)
				client2(newsockfd, msgqid);

			close(newsockfd);
			exit(0);
		} else
			close(newsockfd);

	}

	close(sock_tcp);
	return 0;
}
