#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <errno.h>

#include "messege.pb-c.h"

char const SERVER_NAME[16] = "127.0.0.1";
char const MES1[30] = "Жду сообщений\n";
char const MES2[32] = "Есть сообщение\n";
int const CLIENT_PORT_TCP = 50232;
int const CLIENT_PORT_UDP = 61231;
int const SERVER_PORT_UDP = 60231;
int const SERVER_PORT_TCP = 50231;
int const MAX_STRING = 15;
int const CLIENT_TCP1 = 1;
int const CLIENT_TCP2 = 2;
int const WAIT_T = 6;
int const WORD_LEN = 100;
int const MAX_PORT = 6;
int const SIZE = 100;

void error(const char *msg)
{
	perror(msg);
	exit(0);
}

int init_udp()
{
	int sockfd;	// Socket descriptor
	int portno;	// Port number
	int i = 1;
	int pruf_port;
	struct sockaddr_in serv_addr;	// Client socket structure
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	if (sockfd < 0) {	// Error creating socket
		error("ERROR opening socket");
		exit(EXIT_FAILURE);
	}

	memset((char *) &serv_addr, '\0', sizeof(serv_addr));
	portno = CLIENT_PORT_UDP;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;	// Server accepts connections to all IP addresses
	serv_addr.sin_port = htons(portno);
	pruf_port = bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));	// Binding the socket to the local address

	while(pruf_port < 0 && i <= MAX_PORT) {
		portno = CLIENT_PORT_UDP + i;
		serv_addr.sin_port = htons(portno);
		pruf_port = bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
		i++;
	}

	if(i > MAX_PORT) {
		error("ERROR on binding");
		exit(EXIT_FAILURE);
	}

	return sockfd;
}

int connect_TCP_1(struct sockaddr_in addr)
{
	void *buf;
	Packet mes = PACKET__INIT;
	int sock_tcp;
	char str[SIZE];
	int len, t;
	strcpy(str, "this is string\n");
	t = rand() % WAIT_T;
	len = rand() % (MAX_STRING - 1) + 1;
	sock_tcp = socket(AF_INET, SOCK_STREAM, 0);

	if (sock_tcp < 0) {
		error("ERROR opening socket");
		exit(EXIT_FAILURE);
	}

	addr.sin_port = htons(SERVER_PORT_TCP);

	if (connect(sock_tcp,(struct sockaddr *) &addr,sizeof(addr)) < 0) {
		error("ERROR connecting");
		exit(EXIT_FAILURE);
	}

	printf("Client create message: time = %d, length = %d\n", t, len);
	mes.name.len = len;
	mes.time = t;
	mes.str = malloc(WORD_LEN);
	strncpy(mes.str, str, len);
	printf("string = %s\n",mes.str);
	mes.name.data = str;
	len = packet__get_packed_size(&mes);
	mes.len = len;
	buf = malloc(len);
	packet__pack(&mes, buf);
	send(sock_tcp, &CLIENT_TCP1, sizeof(CLIENT_TCP1), 0);
	send(sock_tcp, buf, len, 0);
	close(sock_tcp);
	sleep(t);
	return 0;
}

int main(int argc, char *argv[])
{
	printf("The first type client is running.\n");
	char buf[SIZE];
	int sock_udp, n;
	struct sockaddr_in serv_addr;
	socklen_t size = sizeof(serv_addr);
	time_t t;
	srand((unsigned) time(&t));
	sock_udp = init_udp();

	while(1) {
		n = recvfrom(sock_udp, (char *)buf, sizeof(char[SIZE]), 0, (struct sockaddr *)&serv_addr, &size);
		buf[n] = '\0';
		printf("The client received the message: %s", buf);

		if(strcmp(buf, MES1) == 0) {
			printf("connect\n");
			connect_TCP_1(serv_addr);
		}

	}

	return 0;
}
