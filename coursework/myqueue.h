#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "messege.pb-c.h"

#define MAX_MSG_SIZE 1024
#define WORD_LEN 100

struct mymsgbuf
{
	char str[WORD_LEN];
	int time;
	int len;
	long mtype;
};

struct mymsgbuf *read_message(int qid, struct mymsgbuf *qbuf, long type)
{
	int bytes;
	qbuf->mtype = sizeof(int) * 2 + WORD_LEN;
	bytes = msgrcv(qid, qbuf,  sizeof(int) * 2 + WORD_LEN , type, MSG_EXCEPT);
	return qbuf;
}

void send_message(int qid, struct mymsgbuf *qbuf, long type, Packet *mess)
{
	qbuf->mtype = type;
	strcpy(qbuf->str, mess->str);
	qbuf->time = mess->time;
	qbuf->len = mess->name.len;

	if((msgsnd(qid, (struct msgbuf *)qbuf, sizeof(int) * 2 + WORD_LEN, 0)) ==-1) {
		printf("Can't send message\n");
		exit(1);
	}

}

int sum_mes_que(int qid)
{
	struct msqid_ds info;
	msgctl(qid, IPC_STAT, &info);
	return  (int)info.msg_qnum;
}
