#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>

pthread_mutex_t mutex_sum = PTHREAD_MUTEX_INITIALIZER;

int size;
int global_sum = 0;
int **matrix;

struct thread_data {
	pthread_t tid;
	int threadno;
	int nthreads;
};

void Init()
{
	matrix = malloc(sizeof(int*) * size);
	for (int i = 0; i < size; i++) {
		matrix[i] = malloc(sizeof(int) * size);
		for (int j = 0; j < size; j++)
			matrix[i][j] = rand() % 2;
	}
}

void Print()
{
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++)
			printf("%d ", matrix[i][j]);
		puts("");
	}
}

void Free()
{
	for (int i = 0; i < size; i++)
		free(matrix[i]);
	free(matrix);
}

void *thread_func(void *arg)
{
	int local_sum = 0;
	struct thread_data *p = (struct thread_data *)arg;
	int str_per_proc = size / p->nthreads;
	int lb = p->threadno * str_per_proc;
	int ub = (p->threadno == p->nthreads - 1) ? (size - 1) : (lb + str_per_proc - 1);
	for (int i = lb; i <= ub; i++)
		for (int j = 0; j < size; j++)
			if (matrix[i][j] == 1)
				local_sum++;
	pthread_mutex_lock(&mutex_sum);
	global_sum += local_sum;
	pthread_mutex_unlock(&mutex_sum);
	printf("It's slave thread number %d. Borders [%d - %d]. Result = %d\n", p->threadno, lb, ub, local_sum);
	return NULL;
}

int main(int argc, char **argv)
{
	puts("Введите количество строк и столбцов в матрице");
	scanf("%d", &size);
	Init();
	Print();
	int nthreads = argc > 1 ? atoi(argv[1]) : 4;

	struct thread_data *tdata = malloc(sizeof(*tdata) * nthreads);
	if (tdata == NULL) {
		fprintf(stderr, "No enough memory\n");
		exit(EXIT_FAILURE);
	}

	printf("Запуск %d потоков\n", nthreads);
	for (int i = 1; i < nthreads; i++) {
		tdata[i].threadno = i;
		tdata[i].nthreads = nthreads;
		if (pthread_create(&tdata[i].tid, NULL, thread_func, &tdata[i]) != 0) {
			fprintf(stderr, "Can't create thread\n");
			exit(EXIT_FAILURE);
		}
	}
	printf("Это мастер поток работает  %u\n", (unsigned int)pthread_self());
	tdata[0].threadno = 0;
	tdata[0].nthreads = nthreads;
	thread_func(&tdata[0]);
	for (int i = 0; i < nthreads; i++)
		pthread_join(tdata[i].tid, NULL);
	free(tdata);
	printf("Сумма всех потоков равна = %d\n", global_sum);
	Free();
	return 0;
}
