#include <stdio.h>
#include <stdlib.h>
#include "test.h"
#include <dlfcn.h>

int main(int argc, char *argv[])
{
        void *library_handler;
        double value = 0;
        double (*powerfunc)(double x, double y);
        library_handler = dlopen("libtest_dynamic.so", RTLD_LAZY);
        if (!library_handler) {
                fprintf(stderr, "dlopen() error: %s\n", dlerror());
        	exit(1);
        }
        powerfunc = dlsym(library_handler, argv[3]);
        puts("Это динамическая библиотека и она динамически загружает функции");
        printf("Выполнилась функция %s и её результат равен - %f\n", argv[3], (*powerfunc)(atoi(argv[1]), atoi(argv[2])));
        dlclose(library_handler);
	return 0;
}
