#include <stdio.h>
#include <stdlib.h>
#include "test.h"
#include <dlfcn.h>

int main(int argc, char *argv[])
{
        if (argc < 3) {
                fprintf(stderr, "Слишком мало агрументов\n");
                exit(1);
        } else 	if (argc == 3) {
		if (argv[0][6] == '1')
			puts("Это статическая библиотека и она подключена во время линковки");
	        else if (argv[0][6] == '2')
			puts("Это динамическая библиотека и она подгружена при компиляции");
		printf("Выполнилось деление - %f\n", div_f(atoi(argv[1]), atoi(argv[2])));
		printf("Выполнилось умножение - %f\n", mul(atoi(argv[1]), atoi(argv[2])));
	}
	return 0;
}
